class PaperModel {
  final String subject;
  final String type;
  final String? from;
  final DateTime since;

  PaperModel(
      {required this.subject,
      required this.type,
      required this.from,
      required this.since});
}
